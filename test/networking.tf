resource "aws_vpc" "sample" {
  cidr_block = "${local.vpc_cidr_block}"
}

resource "aws_route_table" "sample" {
  vpc_id = "${aws_vpc.sample.id}"
}

resource "aws_main_route_table_association" "sample" {
    vpc_id          = "${aws_vpc.sample.id}"
    route_table_id  = "${aws_route_table.sample.id}"
}
