module "test" {
  source                  = "../"

  vpc                     = "${aws_vpc.sample.id}"
  azs                     = "${data.aws_availability_zones.azs.names}"
  cidr_blocks             = "${local.subnets_cidr_blocks}"
  subnet_count            = "${local.subnets_count}"
  route_tables            = ["${aws_route_table.sample.id}"]
  prefix                  = "${local.name}"
  map_public_ip_on_launch = "${local.map_public_ip_on_launch}"
  tags                    = "${local.tags}"
}

output "ids" {
  value = "${module.test.ids}"
}

output "azs" {
  value = "${module.test.azs}"
}

output "cidr_blocks" {
  value = "${module.test.cidr_blocks}"
}

output "route_table_associations" {
  value = "${module.test.route_table_associations}"
}

output "vpc" {
  value = "${module.test.vpc}"
}
