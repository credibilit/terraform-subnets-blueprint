AWS subnets blueprint
===

Creates a set of IPV4 subnets from CIDR blocks and availability zones.

# Sample use

To create a subnet set with this module you need to insert the following peace of code on your own modules:

```
module "test" {
  source          = "git::https://bitbucket.org/credibilit/terraform-subnets-blueprint.git?ref=<VERSION>"

  vpc                     = "..."
  azs                     = [...]
  cidr_blocks             = [...]
  subnet_count            = "..."
  route_tables            = [...]
  prefix                  = "..."
  map_public_ip_on_launch = "..."
  tags                    = {...}
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

## Compatibilities

The version 3.0.0 and above are created to be used with Terraform 0.10 (possibly its compatible with 0.9, but this was not tested). For version 0.8 use version 2.0.0 or the latest version tagged from a commit from 0.8.x branch.

# Input Parameters

The following parameters are used on this module:

- `vpc`: The VPC which the subnets belongs
- `cidr_blocks`: The list of CIDR blocks for the subnets. It must match with a subset of the VPC CIDR.
- `azs`: The list of availability zones to associate the subnets. The sequence will match with the CIDR blocks.
- `prefix`: A prefix to prepend to the subnets names. This module will append a suffix with the AZ name
- `route_tables`: The list of route tables to associate with the subnets. The sequence will match with the CIDR blocks.
- `subnet_count`: How many subnets must be created. This value must be equal or less the CIDR blocks count.
- `map_public_ip_on_launch`: If the subnet must map an public IP to the EC2 instances inside the subnet. Use with caution (Default false).
- `tags`: A map with extra tags (up to 9) to add to the subnets. The Name tag is automatically set and should not be passed here again. (default: {}).

# Output parameters

This are the outputs exposed by this module.

- `ids`: List of ids for the subnets.
- `azs`: List of Availability Zones used by subnets.
- `cidr_blocks`: List of CIDR blocks used by subnets.
- `route_table_associations`: List of route tables associations ids.
- `vpc`: VPC which the subnets belongs.
